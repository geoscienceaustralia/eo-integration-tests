### Introduction

The repo is used to test GA ARD processing.

### What is tested

| Product \ Environment            | ard-pipeline/20211222 | module wagl-5.3.2.gadi |
|----------------------------------|-----------------------|------------------------|
| USGS LS8 C2 L1 RT                | y                     |                        |
| USGS LS8 C2 L1                   | y                     |                        |
| USGS LS7 C2 L1                   | y                     |                        |
| USGS LS7 C2 L1 RT                | y                     |                        |
| USGS LS7 C1 L1 RT                | y                     |                        |
| USGS LS7 C1 L1 T1/T2             | y                     |                        |
| Sinergise S2 L1C                 | y                     |                        |
| ESA S2 L1c (dec 2016 - Mar 2021) | y                     |                        |
| Copernicus S2 L1C baseline 3.00  | not tested            | y - not in these tests |

Is ARD processing successful table. (Generated here; https://www.tablesgenerator.com/markdown_tables# Load readme-table.tgn)



### Execution Notes
If running from NCI:

`go_nci.sh [ls|esa|sinergise]`

Note access to the U46 project is needed and the express queue is used. `go_nci.sh` and `pbs_nci.sh` are a wrapper scripts around the main script `tesp_workflow.sh`.

When running in other environments run the `tesp_workflow.sh` script.  Make a new 'level1' list which is a file with a list of the locations of all the scenes to process. e.g.
```
/g/data/v10/projects/l1_rt_to_c3/test_data/LC80900852021066LGN00_C2_RT
/g/data/v10/projects/l1_rt_to_c3/test_data/LE70980842021066ASA00_C2_RT
/g/data/v10/projects/l1_rt_to_c3/test_data/LE70980842021066ASA00_C1_RT
/g/data/v10/projects/l1_rt_to_c3/test_data/LE71140812021051EDC00_C1_L1
```

Set the environment variable $LANDSAT_LEVEL1_TESTS to point to this file.

### Test Data
The test data is currently stored on AWS;
```
s3://ga-sentinel/test_data/eo-integration-tests-data_vXX.tar.gz
```

## Exporting the test data
```
cd /g/data/v10/projects/l1_rt_to_c3/test_data
tar -cvf /g/data/u46/users/dsg547/dump/eo-integration-tests-data_v4.tar.gz *
cd /g/data/u46/users/dsg547/dump/
export the AWS keys 
. ~/dea.env
aws s3 ls s3://ga-sentinel/ # As a test
aws s3 cp eo-integration-tests-data_v4.tar.gz s3://ga-sentinel/test_data/eo-integration-tests-data_v4.tar.gz
```


### Code base used
Below is an estimate of the code bases used in running the tests.  This is not kept up-to-date though.


| environment \ ARD repo tags | wagl                      | tesp                                                                                   | eodatasets         |   |
|-----------------------------|---------------------------|----------------------------------------------------------------------------------------|--------------------|---|
| NCI - ard-pipeline devv2.1  | wagl-5.5.3.dev19+g02a0d6d | tesp-0.8.1.dev1+g733bbde plus commit ce17bb21401e0e3fbb05710295beb041ad3571a2 applied  | eodatasets3-0.19.0 |   |
|                             |                           |                                                                                        |                    |   |
