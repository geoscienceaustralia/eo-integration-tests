#!/bin/bash

name=$1

if [[ -z "$name" ]]; then
    echo "argument error. Use ls|esa|sinergise"
    exit 1
fi

if [ "$name" = "ls" ]; then
    ARD_ENV_FILE="ard-ls.env"
    LEVEL1_FILE="level1-ls-nci.txt"
elif [ "$name" = "esa" ]; then
    ARD_ENV_FILE="ard-esa.env"
    LEVEL1_FILE="level1-esa.txt"
    #LEVEL1_FILE="level1-esa-softlink.txt"
    #LEVEL1_FILE="level1-esa-yaml.txt"
elif [ "$name" = "sinergise" ]; then
    ARD_ENV_FILE="ard-sinergise.env"
    LEVEL1_FILE="level1-sinergise-nci.txt"
else
    echo "argument error. Use ls|esa|sinergise"
    exit 1
fi

echo $ARD_ENV_FILE
echo $LEVEL1_FILE

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
CURRENT_TIME=$(date "+%Y.%m.%d-%H.%M.%S")

ARD_OUT_DIR=$SCRIPT_DIR/test_"$name"_$CURRENT_TIME

LEVEL1_TESTS=$SCRIPT_DIR/$LEVEL1_FILE

mkdir $ARD_OUT_DIR
mkdir $ARD_OUT_DIR/workdir
mkdir $ARD_OUT_DIR/pkgdir


cd $ARD_OUT_DIR

# from shell variables to environment variables
export SCRIPT_DIR
export CURRENT_TIME
export ARD_OUT_DIR
export LEVEL1_TESTS
export ARD_ENV_FILE
echo $LEVEL1_TESTS
echo $ARD_OUT_DIR

qsub -v SCRIPT_DIR,CURRENT_TIME,ARD_OUT_DIR,LEVEL1_TESTS,ARD_ENV_FILE $SCRIPT_DIR/pbs_nci.sh
