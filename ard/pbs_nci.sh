#!/bin/bash
#PBS -P u46
#PBS -W umask=017
#PBS -q express
#PBS -l walltime=03:00:00,mem=192GB,ncpus=4,jobfs=50GB,other=pernodejobfs
#PBS -l wd
#PBS -l storage=scratch/u46+gdata/u46+scratch/v10+gdata/v10
#PBS -me

#export LEVEL1_TESTS

source $SCRIPT_DIR/$ARD_ENV_FILE

luigid --background --logdir $ARD_OUT_DIR

echo pbs_nci.sh
echo LEVEL1_TESTS
$SCRIPT_DIR/tesp_workflow.sh

