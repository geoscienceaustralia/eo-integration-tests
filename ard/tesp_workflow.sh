#!/bin/bash

if [ -z "$ARD_OUT_DIR" ]; then
    CURRENT_TIME=$(date "+%Y.%m.%d-%H.%M.%S")
    ARD_OUT_DIR=$PWD/test_other_$CURRENT_TIME
fi

echo tesp_workflow.sh

# This needs to be defined
echo $LEVEL1_TESTS # the level1 list

luigi --module tesp.workflow ARDP --level1-list  $LEVEL1_TESTS --workdir $ARD_OUT_DIR/workdir --pkgdir $ARD_OUT_DIR/pkgdir --workers 6 --parallel-scheduling
