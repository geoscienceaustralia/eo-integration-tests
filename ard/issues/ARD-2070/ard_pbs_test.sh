#!/bin/bash

source /g/data/v10/projects/c3_ard/dea-ard-scene-select/scripts/prod/ard_env/prod-wagl-s2.env

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
CURRENT_TIME=$(date "+%Y.%m.%d-%H.%M.%S")

ARD_OUT_DIR=$SCRIPT_DIR/test_$CURRENT_TIME
WORKDIR=$ARD_OUT_DIR/workdir
PKGDIR=$ARD_OUT_DIR/pkgdir
LOGDIR=$ARD_OUT_DIR/logdir

echo $WORKDIR
echo  $LOGDIR
echo $PKGDIR
mkdir -p $WORKDIR
mkdir -p $LOGDIR
mkdir -p $PKGDIR

#ard_pbs --help

ard_pbs --level1-list $SCRIPT_DIR/corrupt_pending_scenes.txt --workdir $WORKDIR  --logdir $LOGDIR  --pkgdir $PKGDIR --env $SCRIPT_DIR/prod-wagl-s2.env --workers 30 --nodes 3 --memory 192 --jobfs 50 --project u46 --queue normal --walltime 04:00:00

#more level-1-pending.txt
#/g/data/fj7/Copernicus/Sentinel-2/MSI/L1C/2020/2020-01/10S145E-15S150E/S2A_MSIL1C_20200119T002701_N0208_R016_T55LGG_20200119T015749.zip
