#!/usr/bin/env python3

# based on
# https://docs.dev.dea.ga.gov.au/incidents/2021-02-23_Broken_FC_NetCDFs_On_NCI.html

# ./given_location_get_id.py corrupt_pending_scenes.txt | cut -d' ' -f1 > ids_to_archive.txt

# datacube -v dataset archive --archive-derived --dry-run $(cat ids_to_archive.txt)
#

# The location looks like this
# 'location': 'zip:/g/data/fj7/Copernicus/Sentinel-2/MSI/L1C/2021/2021-01/30S110E-35S115E/S2B_MSIL1C_20210124T023249_N0209_R103_T50JLL_20210124T035242.zip!/'
import sys
from datacube import Datacube

file_name = sys.argv[1]

dc = Datacube()

with open(file_name) as f:
    for line in f.readlines():
        line = line.strip()
        loc = f'zip:{line}!/'
        dss = dc.index.datasets.get_datasets_for_location(loc, mode='prefix')
        for ds in dss:
            print(f'{ds.id} - {line}')
