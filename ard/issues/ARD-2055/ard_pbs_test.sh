#!/bin/bash


SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
ENV_DIR=$SCRIPT_DIR/prod-wagl-s2.env
ENV_DIR=/g/data/ka08/ga/testing/prod-wagl-s2.env
. $ENV_DIR

#ard_pbs --help
#exit

CURRENT_TIME=$(date "+%Y.%m.%d-%H.%M.%S")

ARD_OUT_DIR=$SCRIPT_DIR/test_$CURRENT_TIME
WORKDIR=$ARD_OUT_DIR/workdir
PKGDIR=$ARD_OUT_DIR/pkgdir
LOGDIR=$ARD_OUT_DIR/logdir

echo $WORKDIR
echo  $LOGDIR
echo $PKGDIR
mkdir -p $WORKDIR
mkdir -p $LOGDIR
mkdir -p $PKGDIR

# Mod the workers and nodes!

ard_pbs --level1-list $SCRIPT_DIR/pending_no_56KQAorV.txt --workdir $WORKDIR  --logdir $LOGDIR  --pkgdir $PKGDIR --env $ENV_DIR --workers 5 --nodes 1 --memory 192 --jobfs 50 --project u46 --queue normal --walltime 03:00:00 --yamls-dir /g/data/ka08/ga/l1c_metadata 

#more level-1-pending.txt
#/g/data/fj7/Copernicus/Sentinel-2/MSI/L1C/2020/2020-01/10S145E-15S150E/S2A_MSIL1C_20200119T002701_N0208_R016_T55LGG_20200119T015749.zip
